package org.zamboua.algorithms.dataStructures.tree;

public class BinaryTree<T> {
    public BinaryTreeNode<T> root;

    public void inOrderTraversal(BinaryTreeNode node){
        if (node != null){
            inOrderTraversal(node.left);
            visit(node);
            inOrderTraversal(node.right);
        }
    }

    public void preOrderTraversal(BinaryTreeNode node){
        if (node != null){
            visit(node);
            preOrderTraversal(node.left);
            preOrderTraversal(node.right);
        }
    }

    public void postOrderTraversal(BinaryTreeNode node){
        if (node != null){
            preOrderTraversal(node.left);
            preOrderTraversal(node.right);
            visit(node);
        }
    }

    private void visit(BinaryTreeNode node){
        System.out.println(node.data);
    }
}

class BinaryTreeNode<T> {
    public BinaryTreeNode left = null;
    public BinaryTreeNode right = null;
    public T data;
}
