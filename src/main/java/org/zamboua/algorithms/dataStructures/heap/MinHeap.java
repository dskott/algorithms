package org.zamboua.algorithms.dataStructures.heap;

import java.util.NoSuchElementException;

public class MinHeap<T extends Comparable<T>> {
    public HeapNode<T> root = null;
    private int size;

    public void insert(T data){
        HeapNode<T> node = new HeapNode<>(data);
        if (root == null){
            root = node;
        } else {
            HeapNode<T> lastNodeParent = getParentForNodeOrder(size);

            // put new node as the child of the lastNodeParent, starting from the left to keep the complete binary tree property
            node.parent = lastNodeParent;
            if (lastNodeParent.leftChild == null) {
                lastNodeParent.leftChild = node;
            } else {
                lastNodeParent.rightChild = node;
            }

            bubbleNodeUp(node);
        }
        size++;
    }

    public T extractMin(){
        if (root == null) throw new NoSuchElementException("The heap is empty");

        T data = root.data;
        if (size == 1){
            root = null;
        } else {
            // remove last node in heap
            HeapNode<T> lastNodeParent = getParentForNodeOrder(size-1);
            HeapNode<T> lastNode;
            if (lastNodeParent.rightChild == null){
                lastNode = lastNodeParent.leftChild;
                lastNodeParent.leftChild = null;
            } else {
                lastNode = lastNodeParent.rightChild;
                lastNodeParent.rightChild = null;
            }
            lastNode.parent = null;

            // put last node at root
            lastNode.leftChild = root.leftChild;
            if (lastNode.leftChild != null) lastNode.leftChild.parent = lastNode;
            lastNode.rightChild = root.rightChild;
            if (lastNode.rightChild != null) lastNode.rightChild.parent = lastNode;
            root = lastNode;

            bubbleNodeDown(root);
        }

        size--;
        return data;
    }

    public T peek(){
        if (root == null) throw new NoSuchElementException("The heap is empty");
        return  root.data;
    }

    public boolean isEmpty(){
        return (size == 0);
    }

    public int getSize() {
        return size;
    }

    /*
    Node order is an integer starting from zero showing the order of appearance of a node,
    if we concatenate the levels of the heap, starting from the smallest level to the highest,
    where each level has nodes from left to right.
    */
    private HeapNode<T> getParentForNodeOrder(int order){
        int nodeLevel = getLastTreeLevel(order + 1);

        /*
        Finding the number of nodes in heap up until the node level means we need to calculate
        $ \sum_{i=0}^{nodeLevel-1}2^i $. We can easily calculate this using the expression
        $ 2^nodeLevel - 1 $, since the two expressions are equal (can be proved by induction).
        */
        int numberOfNodesInHeapPrecedingNodeLevel = (1 << nodeLevel) - 1;

        /*
        Find path from node to the heap root, by specifying the order of each node in the path
        with respect to its level in the tree (i.e. the heap).
         */
        int orderInCurLevel = order - numberOfNodesInHeapPrecedingNodeLevel;
        int[] orderPerLevel = new int[nodeLevel];
        for (int curLevel = nodeLevel-1; curLevel >= 0; curLevel--){
            orderInCurLevel /= 2;
            orderPerLevel[curLevel] = orderInCurLevel;
        }

        // Follow path found above
        HeapNode<T> curParent = root;
        for (int level=1; level<nodeLevel; level++){
            int curOrder = orderPerLevel[level];
            if (curOrder%2 == 0){
                curParent = curParent.leftChild; // leftChild always has even order for its level
            } else {
                curParent = curParent.rightChild; // rightChild always has odd order for its level
            }
        }
        return curParent;
    }

    private int getLastTreeLevel(int treeSize){
        int count = 0;
        while (treeSize > 1){
            count++;
            treeSize /= 2;
        }
        return count;
    }

    private void bubbleNodeUp(HeapNode<T> node){
        while ((node.parent != null) && (node.compareTo(node.parent) < 0)){
            swapParentChild(node.parent, node);
        }
    }

    private void swapParentChild(HeapNode<T> parent, HeapNode<T> child){
        // Make copy of parent
        HeapNode<T> tempParent = new HeapNode<>(parent.data);
        tempParent.parent = parent.parent;
        tempParent.leftChild = parent.leftChild;
        tempParent.rightChild = parent.rightChild;

        // >>>>>>>>>> Move parent to child's position
        parent.parent = child;
        parent.leftChild = child.leftChild;
        if (parent.leftChild != null) parent.leftChild.parent = parent;
        parent.rightChild = child.rightChild;
        if (parent.rightChild != null) parent.rightChild.parent = parent;

        // >>>>>>>>>> Move child to parent's position
        child.parent = tempParent.parent;
        // update child's parent or root
        if (child.parent != null){
            if (child.parent.leftChild == parent){
                child.parent.leftChild = child;
            } else if (child.parent.rightChild == parent){
                child.parent.rightChild = child;
            } else {
                throw new RuntimeException("Parent node's parent does not consider the parent its child");
            }
        } else {
            root = child;
        }

        // update child's new children
        if (tempParent.leftChild == child){
            child.leftChild = parent;
            child.rightChild = tempParent.rightChild;
            if (child.rightChild != null) child.rightChild.parent = child;
        } else if (tempParent.rightChild == child){
            child.rightChild = parent;
            child.leftChild = tempParent.leftChild;
            if (child.leftChild != null) child.leftChild.parent = child;
        } else {
            throw new RuntimeException("Parent node does not consider the child node its child.");
        }
    }

    private void bubbleNodeDown(HeapNode<T> node){
        while (
                ((node.leftChild != null) && (node.compareTo(node.leftChild) > 0)) ||
                        ((node.rightChild != null) && (node.compareTo(node.rightChild) > 0))
        ){
            // find minimum child to become the new parent
            HeapNode<T> minChild = node.leftChild;
            if ((node.rightChild != null) && (minChild.compareTo(node.rightChild) > 0)){
                minChild = node.rightChild;
            }

            swapParentChild(node, minChild);
        }
    }
}

class HeapNode<T extends Comparable<T>> implements Comparable<HeapNode<T>>{
    public HeapNode<T> parent = null;
    public HeapNode<T> leftChild = null;
    public HeapNode<T> rightChild = null;
    public T data;

    public HeapNode(T d){
        data = d;
    }

    @Override
    public int compareTo(HeapNode<T> n) {
        return data.compareTo(n.data);
    }
}

