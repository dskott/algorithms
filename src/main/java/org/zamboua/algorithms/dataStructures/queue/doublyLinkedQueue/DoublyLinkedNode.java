package org.zamboua.algorithms.dataStructures.queue.doublyLinkedQueue;

public class DoublyLinkedNode<T> {
    T data;
    DoublyLinkedNode<T> next = null, prev = null;
    public DoublyLinkedNode(T d){
        this.data = d;
    }

    public T getData(){
        return data;
    }
}
