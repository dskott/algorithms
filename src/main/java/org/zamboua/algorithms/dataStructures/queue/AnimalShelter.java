package org.zamboua.algorithms.dataStructures.queue;

import java.util.NoSuchElementException;

/*
From CTCI, Problem 3.6
 */
public class AnimalShelter {
    private Queue<Dog> dogs = new Queue<>();
    private Queue<Cat> cats = new Queue<>();
    private int lastId = -1;

    public void enqueue(Animal animal){
        lastId++;
        animal.setTimestamp(lastId);

        if (animal instanceof Dog){
            dogs.add((Dog) animal);
        } else if (animal instanceof Cat){
            cats.add((Cat) animal);
        } else {
            throw new RuntimeException("Animal is neither a cat nor a dog.");
        }
    }

    public Animal dequeueAny(){
        if (dogs.isEmpty()){
            return dequeueCat();
        } else if (cats.isEmpty()){
            return dequeueDog();
        } else {
            if (cats.peek().timestamp < dogs.peek().timestamp){
                return dequeueCat();
            } else {
                return dequeueDog();
            }
        }
    }

    public Dog dequeueDog(){
        if (dogs.isEmpty()) throw new NoSuchElementException();
        return dogs.remove();
    }

    public Cat dequeueCat(){
        if (cats.isEmpty()) throw new NoSuchElementException();
        return cats.remove();
    }
}

abstract class Animal {
    String name;
    Integer age;
    Integer timestamp;

    public void setTimestamp(int t){
        this.timestamp = t;
    }

    public Animal(String name, int age){
        this.name = name;
        this.age = age;
    }
}

class Cat extends Animal {

    public Cat(String name, int age) {
        super(name, age);
    }
}

class Dog extends Animal {

    public Dog(String name, int age) {
        super(name, age);
    }
}
