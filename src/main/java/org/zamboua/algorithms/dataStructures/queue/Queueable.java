package org.zamboua.algorithms.dataStructures.queue;

public interface Queueable<T> {
    void add(T item);

    T remove();

    T peek();

    boolean isEmpty();
}
