package org.zamboua.algorithms.dataStructures.queue.lruCache;

public class KeyValuePair<K, V> {
    K key;
    V value;

    public KeyValuePair(K k, V v) {
        key = k;
        value = v;
    }
}
