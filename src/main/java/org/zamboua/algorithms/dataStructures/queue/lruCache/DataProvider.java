package org.zamboua.algorithms.dataStructures.queue.lruCache;

public interface DataProvider<K,V> {
    public V getValueByKey(K key);
    public int getHitsSinceLastCheck();
}
