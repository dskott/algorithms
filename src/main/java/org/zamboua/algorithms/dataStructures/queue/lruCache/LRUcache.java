package org.zamboua.algorithms.dataStructures.queue.lruCache;

import org.zamboua.algorithms.dataStructures.queue.doublyLinkedQueue.DoublyLinkedNode;
import org.zamboua.algorithms.dataStructures.queue.doublyLinkedQueue.DoublyLinkedQueue;

import java.util.*;

/**
 *     Can create an LRU cache by using the following data structures:
 *     - Queue for keeping a record of the order of usage of the items in the queue. The queue will be implemented using
 *     a doubly linked list, and we will store pointers to the head and tail of the queue.
 *     - Hash map for O(1) searching for whether a key exists in the cache, and the value will be a reference to the
 *     node in the queue.
 *
 *     Searching for an item will always go through the hash map. We have 2 scenarios:
 *     - Entry exists in cache: Go to the node in the queue, remove it from the queue, and place it on the head. Finally,
 *     return the data stored in the node.
 *     - Entry does not exist in cache: Go to tail of the queue and pop the least recently used node. Use its key to remove
 *     its reference from the hash map. Fetch the entry from the DB, create a new node and place it at the head of the queue,
 *     add a key-value pair for the new node in the hash map, and serve the data.
 *
 */
public class LRUcache<K,V> {
    private final Map<K, DoublyLinkedNode<KeyValuePair<K,V>>> cacheContents = new HashMap<>();
    private final DoublyLinkedQueue<KeyValuePair<K,V>> recentlyUsedOrder = new DoublyLinkedQueue<>();
    private final int capacity;
    private final DataProvider<K,V> dataProvider;

    public LRUcache(int cap, DataProvider<K,V> dataProvider){
        capacity = cap;
        this.dataProvider = dataProvider;
    }

    public V getValueByKey(K key){
        // check if data exists in cache first
        KeyValuePair<K, V> data;
        if (cacheContents.containsKey(key)){
            data = recentlyUsedOrder.accessNodeRandomlyAndMakeHead(cacheContents.get(key));
        } else {
            data = fetchDataFromUpstreamDataStore(key);

            evictLeastRecentKeyValuePairFromCacheIfNecessary();
            addKeyValuePairToCache(data);
        }
        return data.value;
    }

    public List<K> getKeyUsageOrder(){
        List<K> output = new ArrayList<>();
        List<KeyValuePair<K,V>> order = recentlyUsedOrder.snapshotDataToList();
        for (KeyValuePair<K,V> element: order){
            output.add(element.key);
        }

        return output;
    }

    private void evictLeastRecentKeyValuePairFromCacheIfNecessary(){
        if (recentlyUsedOrder.size() < capacity) return;

        // update both the hashmap and the queue
        K leastRecentlyUsedKey = recentlyUsedOrder.getTail().getData().key;
        cacheContents.remove(leastRecentlyUsedKey);
        recentlyUsedOrder.remove();
    }

    private void addKeyValuePairToCache(KeyValuePair<K,V> pair){
        recentlyUsedOrder.add(pair);
        DoublyLinkedNode<KeyValuePair<K,V>> addedNode = recentlyUsedOrder.getHead();
        cacheContents.put(pair.key, addedNode);
    }

    private KeyValuePair<K,V> fetchDataFromUpstreamDataStore(K key){
        V value = dataProvider.getValueByKey(key);
        if (value == null){
            throw new RuntimeException("Failed to fetch data from upstream for given key.");
        }

        return new KeyValuePair<>(key, value);
    }
}