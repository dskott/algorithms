package org.zamboua.algorithms.dataStructures.queue;

import org.zamboua.algorithms.dataStructures.stack.Stack;

/*
From CTCI, problem 3.4
 */
public class QueueViaStacks<T> implements Queueable<T>{
    private Stack<T> addStack = new Stack<>();
    private Stack<T> removeStack = new Stack<>();

    @Override
    public void add(T item) {
        addStack.push(item);
    }

    @Override
    public T remove() {
        ensureIsRemovableIfPossible();
        return removeStack.pop();
    }

    @Override
    public T peek() {
        ensureIsRemovableIfPossible();
        return removeStack.peek();
    }

    @Override
    public boolean isEmpty() {
        return addStack.isEmpty() && removeStack.isEmpty();
    }

    public int getSize(){
        return addStack.getSize() + removeStack.getSize();
    }

    private void ensureIsRemovableIfPossible(){
        if (removeStack.isEmpty()){
            while (!addStack.isEmpty()){
                removeStack.push(addStack.pop());
            }
        }
    }
}
