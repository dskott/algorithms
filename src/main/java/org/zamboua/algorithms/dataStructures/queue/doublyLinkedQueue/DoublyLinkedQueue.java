package org.zamboua.algorithms.dataStructures.queue.doublyLinkedQueue;

import org.zamboua.algorithms.dataStructures.queue.Queueable;

import java.util.*;

public class DoublyLinkedQueue<T> implements Queueable<T> {
    private DoublyLinkedNode<T> head = null, tail = null;
    private final Set<DoublyLinkedNode<T>> allNodes = new HashSet<>();
    private int size = 0;

    @Override
    public void add(T item) {
        size++;
        DoublyLinkedNode<T> n = new DoublyLinkedNode<>(item);

        if (head != null){
            n.next = head;
            head.prev = n;
        } else {
            tail = n;
        }
        head = n;
        allNodes.add(n);
    }

    @Override
    public T remove() {
        if (tail == null) throw new NoSuchElementException("Queue is empty");
        size--;
        T d = tail.data;
        allNodes.remove(tail);

        if (tail.prev != null){
            tail.prev.next = null;
        } else {
            head = null;
        }
        tail = tail.prev;

        return d;
    }

    @Override
    public T peek() {
        if (tail == null) throw new NoSuchElementException("Queue is empty");
        return tail.data;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    public int size(){
        return size;
    }

    public DoublyLinkedNode<T> getHead(){ return head; }
    public DoublyLinkedNode<T> getTail(){ return tail; }

    public T accessNodeRandomlyAndMakeHead(DoublyLinkedNode<T> node){
        if (!allNodes.contains(node)) throw new NoSuchElementException("The given node is not part of the queue");

        // check if the node is already at the head
        if (node.prev == null) return node.data;

        // this means that the node is not at the head

        // remove node from current position
        node.prev.next = node.next;
        if (node.next != null){
            node.next.prev = node.prev;
        } else {
            tail = node.prev;
        }

        // add node to head
        node.prev = null;
        head.prev = node;
        node.next = head;
        head = node;

        return node.data;
    }

    public List<T> snapshotDataToList(){
        List<T> output = new ArrayList<>();
        DoublyLinkedNode<T> node = head;
        while (node != null){
            output.add(node.getData());
            node = node.next;
        }
        return output;
    }

    public int getNodeIndex(DoublyLinkedNode<T> target){
        if (!allNodes.contains(target)) throw new NoSuchElementException("The given node is not part of the queue");

        int idx = 0;
        DoublyLinkedNode<T> node = head;
        while (node != target){
            node = node.next;
            idx++;
        }
        return idx;
    }
}
