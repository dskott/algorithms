package org.zamboua.algorithms.dataStructures.queue;

import java.util.NoSuchElementException;

public class Queue<T> implements Queueable<T> {
    private static class QueueNode<T> {
        private T data;
        private QueueNode<T> next = null;

        public QueueNode(T data){
            this.data = data;
        }
    }

    private QueueNode<T> first = null;
    private QueueNode<T> last = null;

    @Override
    public void add(T item){
        QueueNode<T> node = new QueueNode<>(item);

        if (last != null){
            last.next = node;
        }
        last = node;

        if (first == null){
            first = last;
        }
    }

    @Override
    public T remove(){
        if (first == null) throw new NoSuchElementException();
        T item = first.data;
        first = first.next;
        if (first == null) last = null;
        return item;
    }

    @Override
    public T peek(){
        if (first == null) throw new NoSuchElementException();
        return first.data;
    }

    @Override
    public boolean isEmpty(){
        return first == null;
    }
}
