package org.zamboua.algorithms.dataStructures.stack;

import java.util.ArrayList;
import java.util.EmptyStackException;

/*
From CTCI, problem 3.3
 */
public class SetOfStacks<T> implements Stackable<T>{
    private ArrayList<StackWithBottom<T>> stacks = new ArrayList<StackWithBottom<T>>();
    private int threshold;

    public SetOfStacks(int threshold){
        if (threshold < 1) throw new RuntimeException("Threshold must be larger than 0.");
        this.threshold = threshold;
    }

    @Override
    public T pop() {
        if (stacks.isEmpty()) throw new EmptyStackException();
        T item = getLastStack().pop();
        removeLastStackIfEmpty();

        return item;
    }

    @Override
    public void push(T item) {
        if (stacks.isEmpty() || (getLastStack().getSize() == threshold)){
            stacks.add(new StackWithBottom<T>());
        }
        getLastStack().push(item);
    }

    @Override
    public T peek() {
        if (stacks.isEmpty()) throw new EmptyStackException();
        return getLastStack().peek();
    }

    @Override
    public boolean isEmpty() {
        return stacks.isEmpty();
    }

    public T popAt(int index){
        T item = stacks.get(index).pop();
        leftShift(index);
        removeLastStackIfEmpty();
        return item;
    }

    private void leftShift(int index){
        for (int i=index; i<stacks.size()-2; i++){
            pushBottomOfNextStack(i);
        }
    }

    private void pushBottomOfNextStack(int index){
        StackWithBottom<T> cur = stacks.get(index);
        StackWithBottom<T> next = stacks.get(index+1);
        cur.push(next.removeBottom());
    }

    private StackWithBottom<T> getLastStack(){
        if (stacks.isEmpty()) throw new EmptyStackException();
        return stacks.get(stacks.size()-1);
    }

    private void removeLastStackIfEmpty(){
        if (getLastStack().isEmpty()) stacks.remove(stacks.size()-1);
    }

}
