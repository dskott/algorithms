package org.zamboua.algorithms.dataStructures.stack;

import java.util.EmptyStackException;

public class Stack<T> implements Stackable<T> {
    private static class StackNode<T>{
        private T data;
        private StackNode<T> next = null;

        public StackNode(T data){
            this.data = data;
        }
    }

    private StackNode<T> top;
    private int size = 0;

    @Override
    public T pop(){
        if (top == null) throw new EmptyStackException();
        T item = top.data;
        top = top.next;
        size--;
        return item;
    }

    @Override
    public void push(T item){
        StackNode<T> t = new StackNode<>(item);
        t.next = top;
        top = t;
        size++;
    }

    @Override
    public T peek(){
        if (top == null) throw new EmptyStackException();
        return top.data;
    }

    @Override
    public boolean isEmpty(){
        return top == null;
    }

    public int getSize(){
        return size;
    }
}
