package org.zamboua.algorithms.dataStructures.stack;

import java.util.EmptyStackException;

public class StackWithBottom<T> implements Stackable<T> {
    // must implement with doubly linked nodes for best performance
    protected static class StackNodeDouble<T>{
        private T data;
        private StackNodeDouble prev, next;

        public StackNodeDouble(T data){
            this.data = data;
            prev = null;
            next = null;
        }
    }

    private StackNodeDouble<T> top, bottom;
    private int size = 0;

    @Override
    public T pop(){
        if (top == null) throw new EmptyStackException();
        size--;
        T item = top.data;
        if (top.next != null) top.next.prev = null;

        top = top.next;
        if (top == null) bottom = null;
        return item;
    }

    @Override
    public void push(T item){
        size++;
        StackNodeDouble<T> t = new StackNodeDouble<>(item);
        if (top == null){
            bottom = t;
        } else {
            t.next = top;
            top.prev = t;
        }
        top = t;
    }

    @Override
    public T peek(){
        if (top == null) throw new EmptyStackException();
        return top.data;
    }

    @Override
    public boolean isEmpty(){
        return top == null;
    }

    public int getSize(){
        return size;
    }

    public T removeBottom(){
        if (top == null) throw new EmptyStackException();
        size--;
        T item = bottom.data;
        if (bottom.prev != null) {
            bottom.prev.next = null;
        } else {
            top = null;
        }
        bottom = bottom.prev;
        return item;
    }
}
