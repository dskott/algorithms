package org.zamboua.algorithms.dataStructures.stack;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/*
From Hackerrank problem "Balanced brackets":
https://www.hackerrank.com/challenges/one-month-preparation-kit-balanced-brackets/problem?h_l=interview&isFullScreen=false&playlist_slugs%5B%5D=preparation-kits&playlist_slugs%5B%5D=one-month-preparation-kit&playlist_slugs%5B%5D=one-month-week-three
 */
public class BalancedBrackets {
    private static Character getExpectedBracket(Character ch){
        switch (ch){
            case ')':
                return '(';
            case ']':
                return '[';
            case '}':
                return '{';
            default:
                throw new RuntimeException("Invalid character");
        }
    }

    /*
     * Complete the 'isBalanced' function below.
     *
     * The function is expected to return a STRING.
     * The function accepts STRING s as parameter.
     */
    public static String isBalanced(String s) {
        Stack<Character> storage = new Stack<>();
        Set<Character> openingBrackets = new HashSet<>(Arrays.asList('(', '{', '['));

        for (int i=0; i<s.length(); i++){
            char ch = s.charAt(i);
            if (openingBrackets.contains(ch)){
                storage.push(ch);
            } else {
                if (storage.isEmpty() || (!storage.pop().equals(getExpectedBracket(ch)))) return "NO";
            }
        }

        if (!storage.isEmpty()){
            return "NO";
        } else {
            return "YES";
        }
    }
}
