package org.zamboua.algorithms.dataStructures.stack;

/*
From CTCI, exercise 3.5
 */
public class SortStack {
    private Stack<Integer> toSort;
    private Stack<Integer> temp = new Stack<>();

    public SortStack(Stack<Integer> stack){
        toSort = stack;
    }

    public Stack<Integer> sort(){
        if (toSort.getSize() <= 1) return toSort;

        while (!toSort.isEmpty()) {
            // find min element
            Integer minElement = toSort.pop();
            int repeated = 1;
            while (!toSort.isEmpty()){
                if (toSort.peek() < minElement ){
                    pushToTempRepeated(minElement, repeated);
                    minElement = toSort.pop();
                    repeated = 1;
                } else if (toSort.peek() == minElement) {
                    repeated++;
                    toSort.pop();
                } else {
                    temp.push(toSort.pop());
                }
            }

            // move everything not sorted yet to toSort
            while (!temp.isEmpty() && (temp.peek() > minElement)){
                toSort.push(temp.pop());
            }

            pushToTempRepeated(minElement, repeated);
        }

        while (!temp.isEmpty()){
            toSort.push(temp.pop());
        }
        return toSort;
    }

    public Stack<Integer> sortV2(){
        if (toSort.getSize() <= 1) return toSort;

        while (!toSort.isEmpty()){
            Integer tmp = toSort.pop();

            while (!temp.isEmpty() && (temp.peek() > tmp)){
                toSort.push(temp.pop());
            }
            temp.push(tmp);
        }

        while (!temp.isEmpty()){
            toSort.push(temp.pop());
        }

        return toSort;
    }

    private void pushToTempRepeated(Integer value, int repeated){
        for (int i=0; i<repeated; i++){
            temp.push(value);
        }
    }
}
