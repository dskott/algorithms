package org.zamboua.algorithms.dataStructures.stack;

/*
From CTCI, problem 3.2
 */
public class StackMin extends Stack<Integer>{
    private final Stack<Integer> minStack = new Stack<>();

    public Integer pop(){
        Integer item = super.pop();
        if (minStack.peek().intValue() == item.intValue()) minStack.pop();
        return item;
    }

    public void push(Integer item){
        if (minStack.isEmpty() || (minStack.peek() >= item)){
            minStack.push(item);
        }
        super.push(item);
    }

    public Integer peek(){
        return super.peek();
    }

    public boolean isEmpty(){
        return super.isEmpty();
    }
}
