package org.zamboua.algorithms.dataStructures.linkedList;

public class LinkedList<T> {
    public Node head;

    public LinkedList(Node<T> node){
        head = node;
    }

    void appendToTail(T data){
        Node end = new Node(data);
        if (head == null){
            head = end;
            return;
        }

        Node n = head;
        while (n.next != null){
            n = n.next;
        }
        n.next = end;
    }

    public Node deleteNode(T data){
        if (head == null) return null;

        Node n = head;
        if (n.data.equals(data)){
            head = n.next;
            return head;
        }

        while (n.next != null){
            if (n.next.data.equals(data)){
                n.next = n.next.next;
            }
            n = n.next;
        }
        return head;
    }

    public int lengthOfList(){
        Node n = head;
        int length = 0;
        while (n != null){
            length++;
            n = n.next;
        }
        return length;
    }

    public Node getKthNode(int k){
        Node n = head;
        while ((k > 0) && (n != null)){
            n = n.next;
            k--;
        }
        return n;
    }
}

class Node<T extends Object> {
    T data;
    Node next = null;

    public Node(T data){
        this.data = data;
    }
}
