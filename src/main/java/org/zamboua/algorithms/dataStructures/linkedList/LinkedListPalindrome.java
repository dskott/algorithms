package org.zamboua.algorithms.dataStructures.linkedList;

public class LinkedListPalindrome {
    // From CTCI, problem 2.6
    public static boolean isPalindrome(LinkedList list){
        int length = list.lengthOfList();
        NodeCheck nc = isPalindrome(list.head, 0, length);
        return nc.valid;
    }

    public static NodeCheck isPalindrome(Node node, int cur, int length){
        NodeCheck nc = new NodeCheck(null);
        int mid = length/2;

        // recurse forwards to reach the middle
        if (cur < mid){
            nc = isPalindrome(node.next, cur+1, length);
        } else {
            nc.n = node;

            // handle case length is even
            if (length % 2 == 0) return nc;
        }

        if (!nc.valid) return nc; // this means that at a previous iteration the list was not meeting the palindrome condition

        if (!node.data.equals(nc.n.data)) nc.valid = false;
        nc.n = nc.n.next; // no need to check if n.next exists, because we only do this for length/2 times
        return nc;
    }
}

class NodeCheck {
    public Node n = null;
    public boolean valid = true;

    public NodeCheck(Node node){
        n = node;
    }
}
