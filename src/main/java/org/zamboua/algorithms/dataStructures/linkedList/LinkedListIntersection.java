package org.zamboua.algorithms.dataStructures.linkedList;

public class LinkedListIntersection {
    // From CTCI, problem 2.7
    public static Node findIntersection(LinkedList list1, LinkedList list2){
        int l1 = list1.lengthOfList();
        int l2 = list2.lengthOfList();
        if ((l1 == 0) || (l2 == 0)) return null;

        Node n1 = list1.head;
        Node n2 = list2.head;
        int diff = l1 - l2;
        if (diff >= 0){
            n1 = list1.getKthNode(diff);
        } else {
            n2 = list2.getKthNode(-diff);
        }

        for (int i=0; i<Math.min(l1, l2); i++){
            if (n1 == n2) return n1;
            n1 = n1.next;
            n2 = n2.next;
        }
        return null;
    }
}
