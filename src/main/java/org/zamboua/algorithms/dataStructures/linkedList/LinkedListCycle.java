package org.zamboua.algorithms.dataStructures.linkedList;

public class LinkedListCycle {
    // From CTCI, problem 2.8

    /*
    This is the implementation given in the book solutions. The idea is we have 2 pointers. One of them moves at
    twice the pace as the other. At some point, they will eventually collide.
     */
    public static Node findBeginningOfCycle(LinkedList list){
        Node secondNode = getCollisionNode(list);
        if (secondNode == null) return null; // check if there is a cycle

        // Now, the head of the list and the collision point are at equal distances from the beginning of the loop.
        // Hence, iterate at the same pace from the beginning of the loop until they collide.
        Node firstNode = list.head;
        while (firstNode != secondNode){
            firstNode = firstNode.next;
            secondNode = secondNode.next;
        }
        return firstNode;
    }

    /**
     * Checks if a linked list has a cycle. If it does it will return one node that is in the cycle. Otherwise,
     * it will return null. Note that the node returned is the collision point of a slow and fast pointer, where the
     * slow moves at a pace of one node, and the fast at two nodes.
     * @param list linked list
     * @return A node in the cycle or null
     */
    public static Node getCollisionNode(LinkedList list){
        Node fast = list.head;
        Node slow = list.head;

        // iterate fast pointer at twice the speed of the second, until they collide
        while ((fast != null) && (fast.next != null)){
            slow = slow.next;
            fast = fast.next.next;
            if (slow == fast) return fast;
        }
        // at this point, if we haven't returned yet, then we reached the end of the list and there is no cycle.
        return null;
    }

    /*
    Alternative solution
     */
    public static Node findBeginningOfCycleV2(LinkedList list){
        Node cycleNode = getCollisionNode(list);
        if (cycleNode == null) return null;

        // find length of cycle
        Node runner = cycleNode.next;
        int cycleLength = 1;
        while (runner != cycleNode){
            runner = runner.next;
            cycleLength++;
        }

        // find idx of cycleNode
        int cycleNodeIdx = 0;
        runner = list.head;
        while (runner != cycleNode){
            runner = runner.next;
            cycleNodeIdx++;
        }

        // Observe this is true:
        // beginningIdx > cycleNodeIdx - cycleLength
        runner = list.head;
        int minIdx = cycleNodeIdx - cycleLength + 1;
        while (minIdx > 0){
            runner = runner.next;
            minIdx--;
        }

        Node leader = cycleNode.next;
        while (minIdx < 0){
            leader = leader.next;
            minIdx++;
        }

        while (runner != leader){
            runner = runner.next;
            leader = leader.next;
        }
        return runner;
    }
}
