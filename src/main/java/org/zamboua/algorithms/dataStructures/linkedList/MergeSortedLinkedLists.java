package org.zamboua.algorithms.dataStructures.linkedList;

public class MergeSortedLinkedLists {
    private static class SmallestNode {
        Node<Integer> node = null;
        boolean firstIsSmallest = true;
    }

    private static SmallestNode getSmallestNode(Node<Integer> head1, Node<Integer> head2){
        SmallestNode result = new SmallestNode();
        if (head1 == null){
            result.node = head2;
            result.firstIsSmallest = false;
        } else if (head2 == null){
            result.node = head1;
        } else {
            if (head1.data < head2.data){
                result.node = head1;
            } else {
                result.node = head2;
                result.firstIsSmallest = false;
            }
        }
        return result;
    }

    public static LinkedList<Integer> mergeLists(LinkedList<Integer> llist1, LinkedList<Integer> llist2) {
        Node<Integer> head1 = llist1.head;
        Node<Integer> head2 = llist2.head;

        SmallestNode result = getSmallestNode(head1, head2);
        Node<Integer> head = result.node;
        Node<Integer> node = head;
        if (result.firstIsSmallest){
            head1 = head1.next;
        } else {
            head2 = head2.next;
        }

        while ((head1 != null) || (head2 != null)){
            result = getSmallestNode(head1, head2);
            node.next = result.node;
            node = node.next;
            if (result.firstIsSmallest){
                head1 = head1.next;
            } else {
                head2 = head2.next;
            }
        }

        return new LinkedList<Integer>(head);
    }
}
