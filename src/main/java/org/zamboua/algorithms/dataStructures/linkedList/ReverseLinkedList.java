package org.zamboua.algorithms.dataStructures.linkedList;

public class ReverseLinkedList<T> {
    public LinkedList<T> reverse(LinkedList<T> linkedList) {

        Node prevHead = linkedList.head, zipper;
        Node reversedHead = null;

        while (prevHead != null){
            zipper = prevHead;
            prevHead = prevHead.next;

            zipper.next = reversedHead;
            reversedHead = zipper;
        }

        return new LinkedList<>(reversedHead);
    }
}
