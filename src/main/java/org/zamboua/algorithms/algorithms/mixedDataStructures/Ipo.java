package org.zamboua.algorithms.algorithms.mixedDataStructures;

import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;

/*
 Problem is from leetCode: https://leetcode.com/problems/ipo/description/
 */
public class Ipo {
    public int findMaximizedCapital(int k, int w, int[] profits, int[] capital) {
        int numProjects = profits.length;
        Project[] projects = new Project[numProjects];
        for (int i=0; i<numProjects; i++){
            projects[i] = new Project(profits[i], capital[i]);
        }
        Arrays.sort(projects);

        // in Java, you can implement a heap using PriorityQueue
        PriorityQueue<Integer> maxHeap = new PriorityQueue<>(Comparator.reverseOrder());

        int curCapital = w;
        int idx = 0; // points to project with the highest capital that we can currently afford

        // complete at most k projects
        for (int i=0; i<k; i++){
            // insert projects we can afford in heap
            while (idx < numProjects) {
                Project proj = projects[idx];
                if (proj.capital <= curCapital) {
                    maxHeap.add(proj.profit);
                    idx++;
                } else {
                    break;
                }
            }

            Integer projProfit = maxHeap.poll();
            if (projProfit == null) {
                break;
            }
            curCapital += projProfit;
        }

        return curCapital;
    }
}

class Project implements Comparable<Project>{
    Integer profit;
    Integer capital;

    Project(int p, int c){
        this.profit = p;
        this.capital = c;
    }

    // sorts in ascending order by capital
    @Override
    public int compareTo(Project project) {
        return this.capital.compareTo(project.capital);
    }
}
