package org.zamboua.algorithms.algorithms.arrays;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/*
From CTCI, problem 16.21
 */
public class SumSwap {
    public static Pair getEqualSumSwapPair(List<Integer> arrA, List<Integer> arrB){
        int sumA = getSum(arrA);
        int sumB = getSum(arrB);

        if ((sumB - sumA)%2 == 1) return null; // in this case, no possible valid pair exists

        // get hash set for fast look-ups in arrB
        Set<Integer> setB = new HashSet<>();
        for (Integer val: arrB){
            setB.add(val);
        }

        for (Integer val: arrA){
            int required = ((sumB - sumA)/2) + val;
            if (setB.contains(required)) {
                return new Pair(val, required);
            }
        }

        // at this point, no valid pair exists, so return null
        return null;
    }

    private static int getSum(List<Integer> arr){
        int sum = 0;
        for (Integer val: arr){
            sum += val;
        }
        return sum;
    }
}

class Pair {
    int valA;
    int valB;

    public Pair(int a, int b){
        valA = a;
        valB = b;
    }
}
