package org.zamboua.algorithms.algorithms.dynamicProgramming;

import java.util.List;

/*
From CTCI, Problem 16.17
 */
public class ContiguousSequence {
    // solve using DP, by finding the largest sequence for an array of size 1 and building up to the input array
    public static int largestContiguousSequenceSum(List<Integer> arr){
        if (arr.size() == 0) throw new RuntimeException("Array has no contents");
        int maxGlobal = arr.get(0);
        int maxRightmost = arr.get(0);

        for (int i=1; i<arr.size(); i++){
            int num = arr.get(i);

            // update rightmost vairables
            if (maxRightmost > 0){
                maxRightmost += num;
            } else {
                maxRightmost = num;
            }

            // update global variables
            if (maxRightmost > maxGlobal) maxGlobal = maxRightmost;
        }

        return maxGlobal;
    }
}
