package org.zamboua.algorithms.algorithms.recursion;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
Hierarchy Flattening
Given a dictionary your task is to flatten the Hierarchy the following ways:
If there is a nested structure the parent key should be concatenated to the child keys.
If there is an array the index of the element should be concatenated to the child keys.
If there is a primitive type the key shouldn't be changed.
Input:
{
'a': 1,
'b': 'foo',
'c': {
 'a': 2,
 'b': 'bar'
},
'd': [1, {'a': 3}]
}
Output:
{
'a': 1,
'b': 'foo',
'c_a': 2,
'c_b' : 'bar',
'd_0': 1,
'd_1_a': 3
}
*/
public class FlattenHierarchy {
    public static Map<String, Object> flatten(Map<String, Object> input){
        return flatten(input, "");
    }

    private static Map<String, Object> flatten(Map<String, Object> input, String currentKey){
        Map<String, Object> output = new HashMap<>();
        for (Map.Entry<String, Object> entry: input.entrySet()){
            String key = entry.getKey();
            Object value = entry.getValue();

            String fullKey = currentKey + key;

            if ((value instanceof String) || (value instanceof Number)){
                output.put(fullKey, value);
            } else if (value instanceof Map) {
                Map<String, Object> recurseOutput = flatten((Map<String, Object>) value, fullKey + "_");
                output.putAll(recurseOutput);
            } else if (value instanceof List){
                Map<String, Object> childMap = new HashMap<>();
                for (int j = 0; j< ((List<?>) value).size(); j++){
                    childMap.put(String.valueOf(j), ((List<?>) value).get(j));
                }
                Map<String, Object> recurseOutput = flatten(childMap, fullKey + "_");
                output.putAll(recurseOutput);
            } else {
                throw new RuntimeException("Values can only be either Strings, or numbers, or Maps or Lists");
            }
        }
        return output;
    }
}
