package org.zamboua.algorithms.algorithms.strings;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CharacterHistograms {
    public static boolean allEqual(Map<Character, Integer> charHist){
        List<Character> keyList = new ArrayList<>();
        keyList.addAll(charHist.keySet());
        Integer firstVal = charHist.get(keyList.get(0));
        for (int i=1; i<keyList.size(); i++){
            if (!firstVal.equals(charHist.get(keyList.get(i)))){
                return false;
            }
        }
        return true;
    }

    public static String isValid(String s) {
        // Write your code here
        int n = s.length();

        Map<Character, Integer> charHist = new HashMap<>();
        for (int i=0; i<n; i++){
            Character ch = s.charAt(i);
            charHist.put(ch, charHist.getOrDefault(ch, 0) + 1);
        }

        // check if all are the same
        if (allEqual(charHist)){
            return "YES";
        }

        Character maxKey = null;
        Integer maxVal = 0;
        Character keyOne = null;
        for (char key: charHist.keySet()){
            Integer val = charHist.get(key);
            if (val > maxVal){
                maxKey = key;
                maxVal = val;
            }
            if (val == 1) keyOne = key;
        }

        charHist.put(maxKey, maxVal-1);
        if (allEqual(charHist)){
            return "YES";
        }
        charHist.put(maxKey, maxVal);

        if (keyOne != null){
            charHist.remove(keyOne);
            if (allEqual(charHist)){
                return "YES";
            }
        }
        return "NO";
    }
}
