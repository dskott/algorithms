package org.zamboua.algorithms.algorithms.strings;

public class Palindromes {
    /*
    Method that checks if it is possible to remove a character from a string to transform into a palindrome.
    If it is possible, return the index of the character that needs to be removed.
     */
    public static int palindromeIndex(String s) {
        // Write your code here
        int n = s.length();
        int left = 0, right = n-1;

        while (left < right){
            if (s.charAt(left) != s.charAt(right)){
                break;
            }
            left++;
            right--;
        }
        if (left >= right) {
            // means that the while loop couldn't find any letter with a mismatch
            return -1;
        }

        // from this point and on, it means we have a mismatch
        int boundLeft = left;
        int boundRight = right;

        // check if palindrome by removing left
        left = boundLeft + 1;
        right = boundRight;
        while (left < right){
            if (s.charAt(left) != s.charAt(right)){
                break;
            }
            left++;
            right--;
        }
        if (left >= right) return boundLeft;

        // check if palindrome by removing right
        left = boundLeft;
        right = boundRight-1;
        while (left < right){
            if (s.charAt(left) != s.charAt(right)){
                break;
            }
            left++;
            right--;
        }
        if (left >= right) return boundRight;

        return -1;
    }
}
