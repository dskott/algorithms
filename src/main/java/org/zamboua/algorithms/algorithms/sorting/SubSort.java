package org.zamboua.algorithms.algorithms.sorting;

import java.util.List;

/*
From CTCI, problem 16.16
 */
public class SubSort {
    public static Range getIndicesRangeToSort(List<Integer> arr){
        int n = arr.size();
        if (n == 1) return new Range(0, 0);

        int[] maxFromLeft = new int[n];
        maxFromLeft[0] = arr.get(0);
        for (int i=1; i<n; i++){
            maxFromLeft[i] = Math.max(maxFromLeft[i-1], arr.get(i));
        }

        int[] minFromRight = new int[n];
        minFromRight[n-1] = arr.get(n-1);
        for (int i=n-2; i>=0; i--){
            minFromRight[i] = Math.min(minFromRight[i+1], arr.get(i));
        }

        int leftIdx = 0;
        for (int i=1; i<n-1; i++){
            if ((arr.get(i) < arr.get(i-1)) || (arr.get(i) > minFromRight[i+1])) break;
            leftIdx++;
        }

        int rightIdx = n-1;
        for (int i=n-2; i>=1; i--){
            if ((arr.get(i)>arr.get(i+1)) || (arr.get(i) < maxFromLeft[i-1])) break;
            rightIdx--;
        }

        // handle case when array is already sorted
        if (leftIdx >= rightIdx) return new Range(leftIdx, leftIdx);

        return new Range(leftIdx, rightIdx);
    }
}

class Range {
    public int leftIdx, rightIdx;

    public Range(int l, int r){
        leftIdx = l;
        rightIdx = r;
    }
}
