package org.zamboua.algorithms.algorithms.sorting;

import java.util.ArrayList;
import java.util.List;

/*
From hackerrank problem "Climbing the Leaderboard", https://www.hackerrank.com/challenges/one-month-preparation-kit-climbing-the-leaderboard/problem?h_l=interview&isFullScreen=false&playlist_slugs%5B%5D=preparation-kits&playlist_slugs%5B%5D=one-month-preparation-kit&playlist_slugs%5B%5D=one-month-week-three
 */
public class ClimbingTheLeaderBoard {
    /*
     * Complete the 'climbingLeaderboard' function below.
     *
     * The function is expected to return an INTEGER_ARRAY.
     * The function accepts following parameters:
     *  1. INTEGER_ARRAY ranked
     *  2. INTEGER_ARRAY player
     */

    public static List<Integer> climbingLeaderboard(List<Integer> ranked, List<Integer> player) {
        // Write your code here
        List<Integer> output = new ArrayList<>();
        if (ranked.size() == 0) {
            for (Integer score: player){
                output.add(1);
            }
            return output;
        }

        int largestRank = 1;
        int smallestScore = ranked.get(0);
        for (int i=1; i<ranked.size(); i++){
            int score = ranked.get(i);
            if (score < smallestScore){
                smallestScore = score;
                largestRank++;
            }
        }

        int rightBound = ranked.size()-1;
        int curRank = largestRank;
        for (Integer score: player){
            while ((rightBound > 0) && (ranked.get(rightBound)<=score)){
                rightBound--;
                if (ranked.get(rightBound)>ranked.get(rightBound+1)){
                    curRank--;
                }
            }

            if (rightBound == 0){
                if (ranked.get(rightBound) > score){
                    output.add(2);
                } else {
                    output.add(1);
                }
            } else {
                output.add(curRank + 1);
            }
        }

        return output;
    }
}

