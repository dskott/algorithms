package org.zamboua.algorithms.algorithms.sorting;

import java.util.Collections;
import java.util.List;

/*
From CTCI, problem 16.6
 */
public class SmallestDifference {
    public static Pair solveBySortingBothArrays(List<Integer> arr1, List<Integer> arr2){
        Collections.sort(arr1);
        Collections.sort(arr2);

        Pair curBest = new Pair(Math.min(arr1.get(0), arr2.get(0)), Math.max(arr1.get(0), arr2.get(0)));
        int smallestDiff = Math.abs(arr1.get(0)-arr2.get(0));

        int p1 = 0, p2 = 0;
        while ((p1 < arr1.size()) && (p2 < arr2.size())){
            int v1 = arr1.get(p1);
            int v2 = arr2.get(p2);
            int absDiff = Math.abs(v1-v2);

            if (absDiff == 0) return new Pair(v1, v2);

            if (absDiff < smallestDiff){
                smallestDiff = absDiff;
                curBest = new Pair(Math.min(v1, v2), Math.max(v1, v2));
            }

            if (v1 > v2){
                p2++;
            } else {
                p1++;
            }
        }
        return curBest;
    }

    public static Pair solveBySortingOneArray(List<Integer> arr1, List<Integer> arr2){

        // find smallest array. This is because we want to sort the smallest array since sorting is expensive
        List<Integer> smallArr, largeArr;
        if (arr1.size() < arr2.size()){
            smallArr = arr1;
            largeArr = arr2;
        } else {
            smallArr = arr2;
            largeArr = arr1;
        }

        Collections.sort(smallArr);
        Pair curBest = new Pair(Math.min(arr1.get(0), arr2.get(0)), Math.max(arr1.get(0), arr2.get(0)));
        int smallestDiff = Math.abs(arr1.get(0)-arr2.get(0));
        for (int i=0; i<largeArr.size(); i++){
            int valA = largeArr.get(i);
            int valB = findClosestNumberByBisection(valA, smallArr);
            int diff = Math.abs(valA-valB);

            if (diff < smallestDiff){
                smallestDiff = diff;
                curBest = new Pair(Math.min(valA, valB), Math.max(valA, valB));
            }

            if (diff == 0) break;
        }

        return curBest;
    }

    private static int findClosestNumberByBisection(int target, List<Integer> sortedArr){
        int l = 0;
        int r = sortedArr.size()-1;

        while (l<r){
            int mid = (r + l)/2;
            int valMid = sortedArr.get(mid);
            if (target == valMid) return valMid;

            if (target > valMid){
                l = mid + 1;
            } else {
                r = mid - 1;
            }
        }

        int closestLeft = getClosestNumberFromSelfAndNeighbours(target, l, sortedArr);
        int closestRight = getClosestNumberFromSelfAndNeighbours(target, r, sortedArr);

        int diffLeft = Math.abs(closestLeft - target);
        int diffRight = Math.abs(closestRight - target);

        if (diffLeft < diffRight){
            return closestLeft;
        } else {
            return closestRight;
        }
    }

    private static int getClosestNumberFromSelfAndNeighbours(int target, int selfIdx, List<Integer> sortedArr){
        int[] vals = new int[3];
        vals[0] = sortedArr.get(Math.max(0, selfIdx-1));
        vals[1] = sortedArr.get(selfIdx);
        vals[2] = sortedArr.get(Math.min(sortedArr.size()-1,selfIdx+1));

        int bestVal = vals[0];
        int bestDiff = Math.abs(vals[0]-target);

        for (int i=1; i<vals.length; i++){
            int diff = Math.abs(target-vals[i]);
            if (diff < bestDiff){
                bestDiff = diff;
                bestVal = vals[i];
            }
        }

        return bestVal;
    }
}

class Pair {
    public int min, max;

    public Pair(int min, int max){
        this.min = min;
        this.max = max;
    }
}
