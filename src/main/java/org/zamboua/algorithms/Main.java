package org.zamboua.algorithms;

import org.zamboua.algorithms.algorithms.recursion.FlattenHierarchy;
import org.zamboua.algorithms.algorithms.strings.CharacterHistograms;
import org.zamboua.algorithms.dataStructures.heap.MinHeap;
import org.zamboua.algorithms.dataStructures.queue.doublyLinkedQueue.DoublyLinkedNode;
import org.zamboua.algorithms.dataStructures.queue.doublyLinkedQueue.DoublyLinkedQueue;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class Main {
    public static void main(String[] args) throws IOException {
        MinHeap<Integer> heap = new MinHeap<>();
        final int[] data = {10, 20, 15, -6, 100, 26, 70, -199, -599, 1000, 2, 66, 208, -28, 49, 4, 3, 6, -23, 74};

        for (Integer num: data){
            heap.insert(num);
        }

        while (!heap.isEmpty()){
            System.out.println(heap.extractMin());
        }
    }

//    public static void runHackerrankTestCases() throws IOException {
//        String testCase = "13";
//        Path pathTotestCases = Paths.get("/home/demetris/IdeaProjects/algorithms/testCases/sherlockAndValidString");
//        Path inputTestCase = pathTotestCases.resolve("input" + testCase + ".txt");
//        Path outputTestCase = pathTotestCases.resolve("output" + testCase + ".txt");
//
//        List<String> inputSubCases = getFileLines(inputTestCase);
//        List<String> results = new ArrayList<>();
//        for (String subCase: inputSubCases){
//            results.add(CharacterHistograms.isValid(subCase));
//        }
//
//        List<String> expectedSubCaseResults = getFileLines(outputTestCase);
//        for (int i=0; i<results.size(); i++){
//            String assessment = "";
//            if (Objects.equals(results.get(i), expectedSubCaseResults.get(i))){
//                assessment = "CORRECT";
//            } else {
//                assessment = "WRONG";
//            }
//            System.out.println(assessment + ", My result: " + results.get(i) + ", Expected result: " + expectedSubCaseResults.get(i));
//        }
//    }
//
//    public static List<String> getFileLines(Path path) throws IOException {
//        List<String> list = new ArrayList<>();
//        try (BufferedReader br = new BufferedReader(new FileReader(path.toFile()))) {
//            String line;
//            while ((line = br.readLine()) != null) {
////                System.out.println(line);
//                list.add(line);
//            }
//        }
//        return list;
//    }
}
