package org.zamboua.algorithms.interviewPreparationKit;

import java.util.Collections;
import java.util.List;

public class Week2Test {
    public static int palindromeIndex(String s) {
        // Write your code here
        int n = s.length();
        int left = 0, right = n-1;

        while (left < right){
            if (s.charAt(left) != s.charAt(right)){
                break;
            }
            left++;
            right--;
        }
        if (left >= right) {
            // means that the while loop couldn't find any letter with a mismatch
            return -1;
        }

        // from this point and on, it means we have a mismatch
        int boundLeft = left;
        int boundRight = right;

        // check if palindrome by removing left
        left = boundLeft + 1;
        right = boundRight;
        while (left < right){
            if (s.charAt(left) != s.charAt(right)){
                break;
            }
            left++;
            right--;
        }
        if (left >= right) return boundLeft;

        // check if palindrome by removing right
        left = boundLeft;
        right = boundRight-1;
        while (left < right){
            if (s.charAt(left) != s.charAt(right)){
                break;
            }
            left++;
            right--;
        }
        if (left >= right) return boundRight;

        return -1;
    }

    public static int getTotalX(List<Integer> a, List<Integer> b) {
        // Write your code here
        int start = Collections.max(a);
        int end = Collections.min(b);

        int count = 0;
        for (int i=start; i<=end; i++){
            boolean fail = false;
            for (int j=0; j<a.size(); j++){
                if (i%a.get(j) != 0){
                    fail = true;
                    break;
                }
            }
            if (fail){
                continue;
            }

            for (int j=0; j<b.size(); j++){
                if (b.get(j)%i != 0){
                    fail = true;
                    break;
                }
            }
            if (fail){
                continue;
            }
            count++;
        }

        return count;
    }

    public static int[] getStringHistogram(String s){
        int[] hist = new int[26];
        for (int i=0; i<s.length(); i++){
            char ch = s.charAt(i);
            hist[(ch-'a')]++;
        }
        return hist;
    }

    public static int anagram(String s) {
        // Write your code here
        int n = s.length();
        if (n%2 == 1) return -1;
        int mid = n/2;

        String leftSub = s.substring(0, mid);
        String rightSub = s.substring(mid, n);
        // System.out.println("leftsub: " + leftSub + " rightsub: " + rightSub);

        int[] leftHist = getStringHistogram(leftSub);
        int[] rightHist = getStringHistogram(rightSub);

        int count = 0;
        for (int i=0; i<26; i++){
            count += Math.abs(leftHist[i]-rightHist[i]);
        }
        return count/2;
    }
}
