package org.zamboua.algorithms.algorithms.recursion;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class FlattenHierarchyTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void flatten() {
        Map<String, Object> input = createTestInput();
        Map<String, Object> output = FlattenHierarchy.flatten(input);

        Map<String, Object> expectedOutput = createTestExpectedOutput();

        assertEquals(expectedOutput, output);
        assertTrue(expectedOutput.equals(output));
    }

    /*
    Input:
    {
    'a': 1,
    'b': 'foo',
    'c': {
     'a': 2,
     'b': 'bar'
    },
    'd': [1, {'a': 3}]
    }
     */
    private static Map<String, Object> createTestInput(){
        Map<String, Object> input = new HashMap<>();
        input.put("a", 1);
        input.put("b", "foo");

        Map<String, Object> tempInput = new HashMap<>();
        tempInput.put("a", 2);
        tempInput.put("b", "bar");
        input.put("c", tempInput);


        Map<String, Object> tempInput2 = new HashMap<>();
        tempInput2.put("a", 3);
        input.put("d", new ArrayList<Object>(
                List.of(1,
                        tempInput2)));
        return input;
    }

    /*
    {
    'a': 1,
    'b': 'foo',
    'c_a': 2,
    'c_b' : 'bar',
    'd_0': 1,
    'd_1_a': 3
    }
     */
    private static Map<String, Object> createTestExpectedOutput(){
        Map<String, Object> output = new HashMap<>();
        output.put("a", 1);
        output.put("b", "foo");
        output.put("c_a", 2);
        output.put("c_b", "bar");
        output.put("d_0", 1);
        output.put("d_1_a", 3);

        return output;
    }
}