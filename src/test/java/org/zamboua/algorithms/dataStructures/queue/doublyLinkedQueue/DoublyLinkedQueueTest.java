package org.zamboua.algorithms.dataStructures.queue.doublyLinkedQueue;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DoublyLinkedQueueTest {
    List<Integer> data = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));

    @Test
    void fullTest(){
        List<Integer> groundTruth = new ArrayList<>();
        DoublyLinkedQueue<Integer> queue = new DoublyLinkedQueue<>();

        addListAndQueue(groundTruth, queue, 1);
        addListAndQueue(groundTruth, queue, 2);
        addListAndQueue(groundTruth, queue, 3);
        DoublyLinkedNode<Integer> nodeThree = queue.getHead();
        addListAndQueue(groundTruth, queue, 4);
        addListAndQueue(groundTruth, queue, 5);
        assert queue.snapshotDataToList().equals(groundTruth);

        moveElementToHeadListAndQueue(groundTruth, queue, nodeThree);
        assert queue.snapshotDataToList().equals(groundTruth);

        addListAndQueue(groundTruth, queue, 6);
        DoublyLinkedNode<Integer> nodeSix = queue.getHead();
        addListAndQueue(groundTruth, queue, 7);
        assert queue.snapshotDataToList().equals(groundTruth);

        removeLastElementListAndQueue(groundTruth, queue);
        removeLastElementListAndQueue(groundTruth, queue);
        moveElementToHeadListAndQueue(groundTruth, queue, nodeSix);
        assert queue.snapshotDataToList().equals(groundTruth);
    }

    @Test
    void add() {
        List<Integer> groundTruth = new ArrayList<>();
        DoublyLinkedQueue<Integer> queue = new DoublyLinkedQueue<>();
        addAllListAndQueue(groundTruth, queue, data);

        assert queue.snapshotDataToList().equals(groundTruth);
    }

    @Test
    void remove() {
        List<Integer> groundTruth = new ArrayList<>();
        DoublyLinkedQueue<Integer> queue = new DoublyLinkedQueue<>();
        addAllListAndQueue(groundTruth, queue, data);

        for (int i=0; i<3; i++){
            removeLastElementListAndQueue(groundTruth, queue);
        }

        assert queue.snapshotDataToList().equals(groundTruth);
    }

    @Test
    void peek() {
        List<Integer> groundTruth = new ArrayList<>();
        DoublyLinkedQueue<Integer> queue = new DoublyLinkedQueue<>();
        addAllListAndQueue(groundTruth, queue, data);

        assert queue.peek() == groundTruth.get(groundTruth.size()-1);
    }

    @Test
    void accessNodeRandomlyAndMakeHead() {
        List<Integer> groundTruth = new ArrayList<>();
        DoublyLinkedQueue<Integer> queue = new DoublyLinkedQueue<>();
        addAllListAndQueue(groundTruth, queue, data);

        // test head and tail
        DoublyLinkedNode<Integer> node1 = queue.getHead();
        moveElementToHeadListAndQueue(groundTruth, queue, node1);
        assert queue.snapshotDataToList().equals(groundTruth);
        DoublyLinkedNode<Integer> node2 = queue.getTail();
        moveElementToHeadListAndQueue(groundTruth, queue, node2);
        assert queue.snapshotDataToList().equals(groundTruth);

        // test from the middle
        addListAndQueue(groundTruth, queue, 101);
        DoublyLinkedNode<Integer> middle = queue.getHead();
        for (int i=102; i<120; i++){
            addListAndQueue(groundTruth, queue, i);
        }
        moveElementToHeadListAndQueue(groundTruth, queue, middle);

        assert queue.snapshotDataToList().equals(groundTruth);
    }

    // helper methods
    private void addAllListAndQueue(List<Integer> list, DoublyLinkedQueue<Integer> queue, List<Integer> toAdd){
        for (Integer num: toAdd){
            addListAndQueue(list, queue, num);
        }
    }

    private void addListAndQueue(List<Integer> list, DoublyLinkedQueue<Integer> queue, Integer num){
        list.add(0, num);
        queue.add(num);
    }

    private void removeLastElementListAndQueue(List<Integer> list, DoublyLinkedQueue<Integer> queue){
        Integer dataList = list.remove(list.size()-1);
        Integer dataQueue = queue.remove();
        assert dataList == dataQueue;
    }

    private void moveElementToHeadListAndQueue(List<Integer> list, DoublyLinkedQueue<Integer> queue, DoublyLinkedNode<Integer> node){
        int idx = queue.getNodeIndex(node);
        Integer dataList = list.remove(idx);
        Integer dataQueue = queue.accessNodeRandomlyAndMakeHead(node);
        list.add(0, dataList);
        assert dataList == dataQueue;
    }
}