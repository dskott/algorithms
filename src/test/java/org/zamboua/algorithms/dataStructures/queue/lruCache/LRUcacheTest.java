package org.zamboua.algorithms.dataStructures.queue.lruCache;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.zamboua.algorithms.dataStructures.queue.doublyLinkedQueue.DoublyLinkedNode;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LRUcacheTest {
    private static final int cacheCapacity = 10;

    private static final DataProvider<String, Integer> dataProvider = new RemoteDataStoreMock();
    private static LRUcache<String, Integer> cache;
    private static LRUcacheNaive<String, Integer> cacheNaive;

    @BeforeEach
    void setUp() {
        cache = new LRUcache<>(cacheCapacity, dataProvider);
        cacheNaive = new LRUcacheNaive<>(cacheCapacity, new RemoteDataStoreMock());

        assertEquals(1, getValueByKeyBothCaches("one"));
        assertEquals(2,getValueByKeyBothCaches("two"));
        assertEquals(3,getValueByKeyBothCaches("three"));
        assertEquals(4,getValueByKeyBothCaches("four"));
        assertEquals(5,getValueByKeyBothCaches("five"));
        assertEquals(6,getValueByKeyBothCaches("six"));
        assertEquals(7,getValueByKeyBothCaches("seven"));
        assertEquals(8,getValueByKeyBothCaches("eight"));

        assertEquals(8,dataProvider.getHitsSinceLastCheck());
        assertEquals(0,dataProvider.getHitsSinceLastCheck());

        assertEquals(cacheNaive.getKeyUsageOrder(),cache.getKeyUsageOrder());
    }

    @AfterEach
    void tearDown() {
    }

    Integer getValueByKeyBothCaches(String key){
        Integer val1 = cache.getValueByKey(key);
        Integer val2 = cacheNaive.getValueByKey(key);
        assertEquals(val1, val2);
        return val1;
    }

    @Test
    void getValueByKey() {
        // get things not yet in cache
        assertEquals(9,getValueByKeyBothCaches("nine"));
        assertEquals(10,getValueByKeyBothCaches("ten"));

        assertEquals(2,dataProvider.getHitsSinceLastCheck());
        // cache is now full

        // get things already in cache
        assertEquals(1,getValueByKeyBothCaches("one"));
        assertEquals(2,getValueByKeyBothCaches("two"));
        assertEquals(3,getValueByKeyBothCaches("three"));
        assertEquals(4,getValueByKeyBothCaches("four"));
        assertEquals(5,getValueByKeyBothCaches("five"));
        assertEquals(0,dataProvider.getHitsSinceLastCheck()); // since the data provider was not hit, we received things from the cache

        // cache queue at the moment:
        // [5, 4, 3, 2, 1, 10, 9, 8, 7, 6]

        assertEquals(cacheNaive.getKeyUsageOrder(), cache.getKeyUsageOrder());

        // get new things not in cache
        assertEquals(11,getValueByKeyBothCaches("eleven"));
        assertEquals(12,getValueByKeyBothCaches("twelve"));
        assertEquals(13,getValueByKeyBothCaches("thirteen"));
        assertEquals(14,getValueByKeyBothCaches("fourteen"));
        assertEquals(15,getValueByKeyBothCaches("fifteen"));
        assertEquals(5, dataProvider.getHitsSinceLastCheck());

        // cache queue at the moment:
        // [15, 14, 13, 12, 11, 5, 4, 3, 2, 1]
        assertEquals(cacheNaive.getKeyUsageOrder(), cache.getKeyUsageOrder());

        // get things already in cache
        assertEquals(5,getValueByKeyBothCaches("five"));
        assertEquals(3,getValueByKeyBothCaches("three"));
        assertEquals(4,getValueByKeyBothCaches("four"));
        assertEquals(1,getValueByKeyBothCaches("one"));
        assertEquals(2,getValueByKeyBothCaches("two"));
        assertEquals(0,dataProvider.getHitsSinceLastCheck());

        // cache queue at the moment:
        // [2, 1, 4, 3, 5, 15, 14, 13, 12, 11]
        assertEquals(cacheNaive.getKeyUsageOrder(), cache.getKeyUsageOrder());

        ArrayList<String> expectedOrder = new ArrayList<>(List.of(new String[]{"two", "one", "four", "three", "five", "fifteen", "fourteen", "thirteen", "twelve", "eleven"}));
        assertEquals(expectedOrder, cache.getKeyUsageOrder());
    }
}

class RemoteDataStoreMock implements DataProvider<String, Integer>{
    private static final Map<String, Integer> data = new HashMap<>();

    static {
        data.put("one", 1);
        data.put("two", 2);
        data.put("three", 3);
        data.put("four", 4);
        data.put("five", 5);
        data.put("six", 6);
        data.put("seven", 7);
        data.put("eight", 8);
        data.put("nine", 9);
        data.put("ten", 10);
        data.put("eleven", 11);
        data.put("twelve", 12);
        data.put("thirteen", 13);
        data.put("fourteen", 14);
        data.put("fifteen", 15);
        data.put("sixteen", 16);
        data.put("seventeen", 17);
        data.put("eighteen", 18);
        data.put("nineteen", 19);
        data.put("twenty", 20);
    }

    private int hits ;

    public RemoteDataStoreMock(){
        hits = 0;
    }

    @Override
    public Integer getValueByKey(String key) {
        hits++;
        return data.get(key);
    }

    @Override
    public int getHitsSinceLastCheck(){
        int output = hits;
        hits = 0;
        return output;
    }
}

class LRUcacheNaive<K,V> {
    private final List<KeyValuePair<K,V>> recentlyUsedOrder = new ArrayList<>();
    private final int capacity;
    private final DataProvider<K,V> dataProvider;

    public LRUcacheNaive(int cap, DataProvider<K,V> dataProvider){
        capacity = cap;
        this.dataProvider = dataProvider;
    }

    public V getValueByKey(K key){
        // check if key is in cache
        int idx = searchForKey(key);
        KeyValuePair<K,V> data;
        if (idx >=0){
            data = recentlyUsedOrder.get(idx);
            recentlyUsedOrder.remove(idx);
        } else {
            data = fetchDataFromUpstreamDataStore(key);

            evictLeastRecentKeyValuePairFromCacheIfNecessary();
        }

        addKeyValuePairToCache(data);
        return data.value;
    }

    public List<K> getKeyUsageOrder(){
        List<K> output = new ArrayList<>();
        for (KeyValuePair<K,V> element: recentlyUsedOrder){
            output.add(element.key);
        }
        return output;
    }

    private int searchForKey(K key){
        for (int i=0; i<recentlyUsedOrder.size(); i++){
            if (recentlyUsedOrder.get(i).key.equals(key)) return i;
        }
        return -1;
    }

    private void evictLeastRecentKeyValuePairFromCacheIfNecessary(){
        if (recentlyUsedOrder.size() < capacity) return;

        // update both the hashmap and the queue
        recentlyUsedOrder.remove(recentlyUsedOrder.size()-1);
    }

    private void addKeyValuePairToCache(KeyValuePair<K,V> pair){
        recentlyUsedOrder.add(0, pair);
    }

    private KeyValuePair<K,V> fetchDataFromUpstreamDataStore(K key){
        V value = dataProvider.getValueByKey(key);
        if (value == null){
            throw new RuntimeException("Failed to fetch data from upstream for given key.");
        }

        return new KeyValuePair<>(key, value);
    }
}