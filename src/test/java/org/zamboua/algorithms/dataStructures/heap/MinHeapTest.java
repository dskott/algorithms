package org.zamboua.algorithms.dataStructures.heap;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.zamboua.algorithms.dataStructures.queue.doublyLinkedQueue.DoublyLinkedQueue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MinHeapTest {
    private MinHeap<Integer> heap = new MinHeap<>();
    private List<Integer> groundTruth = new ArrayList<>();
    private static final int[] data = {10, 20, 15, -6, 100, 26, 70, -199, -599, 1000, 2, 66, 208, -28, 49, 4, 3, 6, -23, 74};

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void fullTest1() {
        for (Integer num: data){
            insertListAndHeap(groundTruth, heap, num);
        }

        while (!heap.isEmpty()){
            extractMinListAndHeap(groundTruth, heap);
        }
    }

    @Test
    void fullTest2() {
        for (int i=3; i<12; i++){
            insertListAndHeap(groundTruth, heap, data[i]);
        }

        for (int i=0; i<5; i++){
            extractMinListAndHeap(groundTruth, heap);
        }

        for (Integer num: data){
            insertListAndHeap(groundTruth, heap, num);
        }

        while (!heap.isEmpty()){
            extractMinListAndHeap(groundTruth, heap);
        }
    }

    @Test
    void repeatedNumbersTest(){
        int repeated = 7;
        for (int i=0; i<5; i++){
            insertListAndHeap(groundTruth, heap, repeated);
        }

        for (Integer num: data){
            insertListAndHeap(groundTruth, heap, num);
        }

        while (!heap.isEmpty()){
            extractMinListAndHeap(groundTruth, heap);
        }
    }

    @Test
    void smallHeapTest(){
        // heap with only 2 levels
        for (int i=0; i<3; i++){
            insertListAndHeap(groundTruth, heap, data[i]);
        }
        while (!heap.isEmpty()){
            extractMinListAndHeap(groundTruth, heap);
        }

        // heap with only 1 level
        insertListAndHeap(groundTruth, heap, -10);
        extractMinListAndHeap(groundTruth, heap);
    }

    // helper methods
    private void insertListAndHeap(List<Integer> list, MinHeap<Integer> heap, Integer num){
        heap.insert(num);
        list.add(num);
    }

    private void assertListAndHeapHaveSameMin(List<Integer> list, MinHeap<Integer> heap){
        assertEquals(Collections.min(list), heap.peek());
    }

    private void extractMinListAndHeap(List<Integer> list, MinHeap<Integer> heap){
        Integer minList = Collections.min(list);
        int minIndex = list.indexOf(minList);
        list.remove(minIndex);

        Integer minHeap = heap.extractMin();

        assertEquals(minList, minHeap);
    }
}